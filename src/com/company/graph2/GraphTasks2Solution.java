package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> vertsAndLengthToThem = new HashMap<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertsAndLengthToThem.put(i, Integer.MAX_VALUE);
        }
        vertsAndLengthToThem.put(startIndex, 0);

        ArrayDeque<Integer> mustBeVisited = new ArrayDeque<>();
        mustBeVisited.addFirst(startIndex);
        while (mustBeVisited.size()!=0) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (i!=startIndex && adjacencyMatrix[startIndex][i] > 0 &&
                        !mustBeVisited.contains(i) && vertsAndLengthToThem.get(i) > (adjacencyMatrix[startIndex][i]+vertsAndLengthToThem.get(startIndex))){
                    mustBeVisited.addLast(i);
                    vertsAndLengthToThem.put(i, (vertsAndLengthToThem.get(startIndex)+adjacencyMatrix[startIndex][i]));
                }
            }
            mustBeVisited.pollFirst();
            if (mustBeVisited.size()!=0){
                startIndex = mustBeVisited.getFirst();
            }
        }
        return vertsAndLengthToThem;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        ArrayDeque<Integer> vertsInTree = new ArrayDeque<>();
        vertsInTree.add(0);

        int min = Integer.MAX_VALUE;
        int vertWithMinLength = 0;
        int sum = 0;
        int t;

        while (vertsInTree.size() != adjacencyMatrix.length) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if((adjacencyMatrix[vertsInTree.getFirst()][i]!=0 || adjacencyMatrix[vertsInTree.getLast()][i]!=0) && !vertsInTree.contains(i)){
                    if(adjacencyMatrix[vertsInTree.getFirst()][i]==0){
                        t =adjacencyMatrix[vertsInTree.getLast()][i];
                    } else{
                        if(adjacencyMatrix[vertsInTree.getLast()][i]==0){
                            t = adjacencyMatrix[vertsInTree.getFirst()][i];
                        }
                        else{
                            t = Math.min(adjacencyMatrix[vertsInTree.getFirst()][i], adjacencyMatrix[vertsInTree.getLast()][i]);
                        }
                    }
                    if(t < min){
                        min = t;
                        vertWithMinLength = i;
                    }
                }
            }
            if(adjacencyMatrix[vertsInTree.getFirst()][vertWithMinLength]==min){
                vertsInTree.addFirst(vertWithMinLength);
            } else {
                if(adjacencyMatrix[vertsInTree.getLast()][vertWithMinLength]==min){
                    vertsInTree.addLast(vertWithMinLength);
                }
            }
            sum=sum+min;
            min = Integer.MAX_VALUE;
        }
        return sum;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
